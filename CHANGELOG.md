# Changelog

All notable changes to this project will be documented in this file. See [standard-version](https://github.com/conventional-changelog/standard-version) for commit guidelines.

### [5.1.1](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v5.1.0...v5.1.1) (2019-11-16)

## [5.1.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v5.0.0...v5.1.0) (2019-09-27)

## [5.0.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v4.0.0...v5.0.0) (2019-09-26)

## [4.0.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.7...v4.0.0) (2019-09-26)

### [3.1.7](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.6...v3.1.7) (2019-09-23)

### [3.1.6](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.5...v3.1.6) (2019-09-22)

### [3.1.5](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.4...v3.1.5) (2019-09-22)

### [3.1.4](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.3...v3.1.4) (2019-09-22)

### [3.1.3](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.2...v3.1.3) (2019-09-22)

### [3.1.2](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.1...v3.1.2) (2019-09-22)

### [3.1.1](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.1.0...v3.1.1) (2019-09-22)

## [3.1.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v3.0.0...v3.1.0) (2019-09-22)

## [3.0.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.6...v3.0.0) (2019-09-22)

### [2.0.6](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.5...v2.0.6) (2019-09-22)

### [2.0.5](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.4...v2.0.5) (2019-09-21)

### [2.0.4](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.3...v2.0.4) (2019-09-21)

### [2.0.3](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.2...v2.0.3) (2019-09-20)

### [2.0.2](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.1...v2.0.2) (2019-09-20)

### [2.0.1](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v2.0.0...v2.0.1) (2019-09-20)

## [2.0.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.5...v2.0.0) (2019-09-20)

### [1.2.5](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.4...v1.2.5) (2019-09-20)

### [1.2.4](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.3...v1.2.4) (2019-09-20)

### [1.2.3](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.2...v1.2.3) (2019-09-20)

### [1.2.2](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.1...v1.2.2) (2019-09-16)

### [1.2.1](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.2.0...v1.2.1) (2019-09-16)

## [1.2.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v1.1.0...v1.2.0) (2019-09-13)

## [1.1.0](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v0.0.5...v1.1.0) (2019-09-13)

### [0.0.5](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v0.0.4...v0.0.5) (2019-09-13)

### [0.0.4](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v0.0.3...v0.0.4) (2019-09-13)

### [0.0.3](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v0.0.2...v0.0.3) (2019-09-13)

### [0.0.2](https://gitlab.com/gurukulartsdev/dtpwa-template/compare/v0.0.1...v0.0.2) (2019-09-13)

### 0.0.1 (2019-09-13)
