import { Component, OnInit } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';

@Component({
  selector: 'app-nav-left',
  templateUrl: './nav-left.component.html',
  styleUrls: ['./nav-left.component.scss']
})
export class NavLeftComponent implements OnInit {
  
  constructor(public $ : Utilities) { }

  ngOnInit() {
  }

}
