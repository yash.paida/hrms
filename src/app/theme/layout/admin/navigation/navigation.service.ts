import { Injectable } from '@angular/core';
import { Collection } from 'src/app/core/helper.service';
import { BehaviorSubject } from 'rxjs';

export interface NavigationItem {
  id: string;
  title: string;
  type: 'item' | 'collapse' | 'group';
  translate?: string;
  icon?: string;
  hidden?: () => boolean;
  url?: string;
  classes?: string;
  exactMatch?: boolean;
  external?: boolean;
  target?: boolean;
  breadcrumbs?: boolean;
  parent?: string;
  function?: () => any;
  badge?: () => { title?: string; type?: string; };
  children?: Navigation[];
}

export interface Navigation extends NavigationItem {
  children?: NavigationItem[];
}

@Injectable()
export class NavigationService {
  private _navigation: NavigationItem  = { id: 'root', title: 'root', type: 'group', children: [] };
  private Items: Collection<NavigationItem> = new Collection<NavigationItem>("id");
  public Rendered$: BehaviorSubject<Navigation[]> = new BehaviorSubject<Navigation[]>(this._navigation.children);

  get NavigationItems(): Navigation[] {
    return this._navigation.children;
  }
  set NavigationItems(Nav: Navigation[]) {
    //let nav: NavigationItem = { id: 'root', title: 'root', type: 'group', children: Nav };
    //this._navigation = nav;
    let nav = Object.assign([],Nav);
    this.SetItems(nav);
  }

  private SetItems(nav: Navigation[], parent: string = 'inherit') {
    if(!nav) return false;
    for (let index = 0; index < nav.length; index++) {
      const n = nav[index];
      n.parent = (parent == 'inherit' ? n.parent : parent) || 'root';
      const ncopy = Object.assign({}, n);
      ncopy.children = [];
      this.Items.set(ncopy.id, ncopy);
      this.SetItems(n.children, n.id);
    }
  }

  getById(id: string, nav: NavigationItem = this._navigation) {
    if (nav.id == id) {
      return nav;
    } else if (nav.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < nav.children.length; i++) {
        result = this.getById(id, nav.children[i]);
      }
      return result;
    }
    return null;
  }

  getByUrl(url: string, nav: NavigationItem = this._navigation) {
    if (nav.url == url) {
      return nav;
    } else if (nav.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < nav.children.length; i++) {
        result = this.getByUrl(url, nav.children[i]);
      }
      return result;
    }
    return null;
  }

  private push(navitem: NavigationItem, parent: string = 'root', nav: NavigationItem = this._navigation) {
    if (nav.id == parent) {
      const ncopy = Object.assign({}, navitem);
      ncopy.children = [];
      nav.children.push(ncopy);
      return true;
    } else if (nav.children != null) {
      var i;
      var result = null;
      for (i = 0; result == null && i < nav.children.length; i++) {
        result = this.push(navitem, parent, nav.children[i]);
      }
      return result;
    }
    return null;
  }

  render() {
    this._navigation  = { id: 'root', title: 'root', type: 'group', children: [] };
    this.Items.values().forEach(n => {
      this.push(n,n.parent)
    });
    this.Rendered$.next(this.NavigationItems);
  }

  reset() {
    this.Items.clear();
    this.render();
  }
}
