import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';
import { MediaSizes } from 'src/app/core/ui.service';

@Component({
  selector: 'app-nav-search',
  templateUrl: './nav-search.component.html',
  styleUrls: ['./nav-search.component.scss']
})
export class NavSearchComponent implements OnInit {
  @Input() public searchMaxWidth: number = 170;
  @Input() public defaultOpened: boolean = false;
  @Input() public fillNavBar: boolean = true;
  @Output() onOpen = new EventEmitter();
  @Output() onClose = new EventEmitter();
  @Output() onSearch = new EventEmitter();


  private _isOpen: boolean;
  public searchInterval: any;
  public searchWidth: number;
  public searchWidthString: string;

  private _searchAvailableWidth: number;



  constructor(public $: Utilities) {
    this.searchWidth = 0;
  }

  ngOnInit() {
    setTimeout(() => {
      if (this.defaultOpened == true) this.searchOn();
    }, 2000);

  }

  search() {

  }

  searchOn() {
    this._searchAvailableWidth = document.querySelector('#main-navbar').clientWidth - (document.querySelector('#navbar-title').clientWidth + document.querySelector('#navbar-left').clientWidth + document.querySelector('#navbar-right').clientWidth);
    //console.log(document.querySelector('#main-navbar').clientWidth,document.querySelector('#navbar-title').clientWidth,document.querySelector('#navbar-left').clientWidth,document.querySelector('#navbar-right').clientWidth)
    let maxWidth = Math.min(this.searchMaxWidth, (this.$.UI.Device.size == MediaSizes.xs ? 10000 : this._searchAvailableWidth - 100), this.$.UI.Device.width - 210);
    //console.log(maxWidth, this.$.UI.Device.size , MediaSizes.xs);
    if (this._isOpen) {
      this.onSearch.emit(true);
      return false;
    }
    if (this.fillNavBar) document.querySelector('#main-search').closest('.navbar').classList.add('search-opened')
    document.querySelector('#main-search').classList.add('open');
    this.searchInterval = setInterval(() => {
      if (this.searchWidth >= maxWidth) {
        clearInterval(this.searchInterval);
        this.onOpen.emit(this.searchWidth);
        this._isOpen = true;
        setTimeout(() => {
          document.querySelector('#main-search').closest('.navbar').classList.add('fill');
        }, 250);
        return false;
      }
      this.searchWidth = this.searchWidth + 30;
      this.searchWidthString = this.searchWidth + 'px';
    }, 35);
  }

  searchOff() {
    document.querySelector('#main-search').closest('.navbar').classList.remove('fill');
    this.searchInterval = setInterval(() => {
      this.searchWidth = this.searchWidth - 30;
      this.searchWidthString = this.searchWidth + 'px';
      if (this.searchWidth <= 0) {
        setTimeout(() => {
          this.onClose.emit(true);
          this._isOpen = false;
          document.querySelector('#main-search').closest('.navbar').classList.remove('search-opened')
          document.querySelector('#main-search').classList.remove('open');
        }, 250);
        clearInterval(this.searchInterval);
        return false;
      }
    }, 35);
  }

}
