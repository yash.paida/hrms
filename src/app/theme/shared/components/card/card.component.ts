import {Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {NgbDropdownConfig} from '@ng-bootstrap/ng-bootstrap';
import {AnimationBuilder, AnimationService} from 'css-animator';
import {animate, AUTO_STYLE, state, style, transition, trigger} from '@angular/animations';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

@Component({
  selector: 'app-card',
  templateUrl: './card.component.html',
  styleUrls: ['./card.component.scss'],
  providers: [NgbDropdownConfig],
  animations: [
    trigger('collapsedCard', [
      state('collapsed, void',
        style({
          overflow: 'hidden',
          height: '0px',
        })
      ),
      state('expanded',
        style({
          //overflow: 'hidden',
          height: AUTO_STYLE,
        })
      ),
      transition('collapsed <=> expanded', [
        animate('400ms ease-in-out')
      ])
    ]),
    trigger('cardRemove', [
      state('open', style({
        opacity: 1
      })),
      state('closed', style({
        opacity: 0,
        display: 'none'
      })),
      transition('open <=> closed', animate('400ms')),
    ])
  ]
})

export class CardComponent implements OnInit {
  @Input() cardTitle: string;
  @Input() cardClass: string;
  @Input() defaultOpened = true;
  @Input() blockClass: string;
  @Input() headerClass: string;
  @Input() collapsible: boolean;
  @Input() maximizable: boolean;
  @Input() hidHeader: boolean;
  @Input() customHeader: boolean;

  @Output() onRefresh = new EventEmitter<Subject<boolean>>();
  @Output() onDelete = new EventEmitter<Subject<boolean>>();

  private Refreshed$ = new Subject<boolean>();
  private Deleted$ = new Subject<boolean>();

  public options: boolean;
  public animation: string;
  public fullIcon: string;
  public isAnimating: boolean;
  public animator: AnimationBuilder;
  public animators: AnimationBuilder;

  public collapsedCard: string;
  public collapsedIcon: string;

  public loadCard: boolean;
  public cardRemove: string;
  
  public showRefresh: Boolean;
  public showDelete: Boolean;

  constructor(animationService: AnimationService, config: NgbDropdownConfig) {
    config.placement = 'bottom-right';
    this.customHeader = false;
    this.options = true;
    this.maximizable = true;
    this.collapsible = true;
    this.hidHeader = false;
    this.cardTitle = 'Card Title';

    this.animator = animationService.builder();
    this.animators = animationService.builder();
    this.animator.useVisibility = true;
    this.fullIcon = 'icon-maximize-2';
    this.isAnimating = false;

    this.collapsedCard = 'expanded';
    this.collapsedIcon = 'icon-chevron-up';

    this.loadCard = false;

    this.cardRemove = 'open';

  }

  ngOnInit() {
    if(this.defaultOpened == false) this.collapsedCardToggle(true);
    this.Refreshed$.subscribe(state => {
      this.loadCard = !state
      if(!state) this.cardClass = 'card-load';
      else this.cardClass = 'expanded';
    })

    this.Deleted$.subscribe(state => {
      this.loadCard = !state
      if(!state) this.cardClass = 'card-load';
      else {
        this.cardClass = 'expanded';
        this.cardRemove = this.cardRemove === 'closed' ? 'open' : 'closed';
      }
    })

    this.showRefresh = this.onRefresh.observers.length > 0;
    this.showDelete = this.onDelete.observers.length > 0;
    this.options = (Boolean)(this.collapsible  || this.maximizable || this.showDelete || this.showRefresh);
    if (!this.options || this.hidHeader || this.customHeader) this.collapsedCard = 'false';

  }

  public fullCardToggle(element: HTMLElement, animation: string, status: boolean) {
    animation = this.cardClass === 'full-card' ? 'zoomOut' : 'zoomIn';
    this.fullIcon = this.cardClass === 'full-card' ? 'icon-maximize-2' : 'icon-minimize-2';
    // const duration = this.cardClass === 'full-card' ? 300 : 600;
    this.cardClass = this.cardClass === 'full-card' ? this.cardClass : 'full-card';
    if (status) {
      this.animation = animation;
    }
    this.isAnimating = true;

    this.animators
      .setType(this.animation)
      .setDuration(500)
      .setDirection('alternate')
      .setTimingFunction('cubic-bezier(0.1, -0.6, 0.2, 0)')
      .animate(element)
      .then(() => {
        this.isAnimating = false;
      })
      .catch(e => {
        this.isAnimating = false;
      });
    setTimeout(() => {
      this.cardClass = animation === 'zoomOut' ? '' : this.cardClass;
      if (this.cardClass === 'full-card') {
        //document.querySelector('body').style.overflow = 'hidden';
      } else {
        document.querySelector('body').removeAttribute('style');
      }
    }, 500);
  }

  collapsedCardToggle(event) {
    if(this.collapsible) 
    {
      this.collapsedCard = this.collapsedCard === 'collapsed' ? 'expanded' : 'collapsed';
      this.collapsedIcon = this.collapsedCard === 'collapsed' ? 'icon-chevron-down' : 'icon-chevron-up';
    }
  }

  cardRefresh() {
    this.Refreshed$.next(false);
    this.onRefresh.emit(this.Refreshed$);
  }

  cardRemoveAction(event) {
    this.Deleted$.next(false);
    this.onDelete.emit(this.Deleted$);
  }

}
