import {Component, Input, OnInit, EventEmitter, Output} from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-alert',
  templateUrl: './alert.component.html',
  styleUrls: ['./alert.component.scss']
})
export class AlertComponent implements OnInit {
  @Input() type: string;
  @Input() data: object;
  @Input() dismiss: string;
  @Output() onDismiss = new EventEmitter<object>();

  constructor() { }

  ngOnInit() {
  }

  public dismissAlert(element) {
    this.onDismiss.emit(this.data);
    element.remove();
  }

}
