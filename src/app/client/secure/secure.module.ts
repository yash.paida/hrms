import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SecureComponent } from './secure.component';
import { SecureRoutingModule } from './secure-routing.module';
import { AccessGuard, CanDeactivateGuard } from 'src/app/core/access.guard';

@NgModule({
  imports: [
    CommonModule,
    SecureRoutingModule,
  ],
  providers: [
    AccessGuard,
    CanDeactivateGuard
  ],
  declarations: [SecureComponent]
})
export class SecureModule {}

/* Application Specs */

/*  Mandatory enums imported to Access Service
    Use AppRoles, AppPermissions, AppScopes from AccessService for strings.
 */
export enum Roles { Admin, User, Public }
export enum Permissions { Can_Manage, Can_Read, Can_Log, Can_Manage_Package }
export enum Scopes { Branch, Unit }

/* Application Specific enums can be maintained here */

export enum DivisionTypes { "Organization", "Territory", "Zone", "Region", "Branch" }

