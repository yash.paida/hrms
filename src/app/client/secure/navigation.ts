import { Navigation } from "src/app/theme/layout/admin/navigation/navigation.service";

export const AppNavigation : Navigation[] = [
    {
      id: 'navigation',
      title: 'Navigation',
      type: 'group',
      icon: 'icon-navigation',
      children: [
      
      ]
    }
  ];