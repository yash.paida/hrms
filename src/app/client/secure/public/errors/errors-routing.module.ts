import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CommonModule } from '@angular/common';
import { ErrorsComponent } from './errors.component';

const routes: Routes = [
    { path: '', component: ErrorsComponent }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class ErrorsRoutingModule {}
