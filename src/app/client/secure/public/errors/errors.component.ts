import { Component, OnInit } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';

@Component({
  selector: 'app-errors',
  templateUrl: './errors.component.html',
  styleUrls: ['./errors.component.scss']
})
export class ErrorsComponent implements OnInit {

  public Title: string;
  public Message: string;
  

  constructor(public $ : Utilities) {
  }
  
  ngOnInit() {
    this.setMessage("Sorry","Looks like you have been here by Mistake");
    this.$.Helper.thisRoute.queryParams.subscribe(params => {    
      this.setMessage(params['error'],params['error_reason']);
    })
  }

  private setMessage(Title: string = "Sorry", Msg: string = "Looks like you have been here by Mistake"){
    this.Title = Title;
    this.$.UI.PageTitle = this.Title;
    this.Message = Msg;    
  }

}
