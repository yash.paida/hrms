import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AccessControlComponent } from './accessControl.component';
import { AccessControlRoutingModule } from './accessControl-routing.module';
import { RolesComponent } from './roles/roles.component';

@NgModule({
  imports: [
    CommonModule,
    AccessControlRoutingModule,
  ],
  declarations: [AccessControlComponent,RolesComponent]
})
export class AccessControlModule { }
