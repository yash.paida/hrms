import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from 'src/app/core/access.guard';
import { RolesComponent } from './roles/roles.component';

const routes: Routes = [
  {
    path: "roles",
    component: RolesComponent,
    canDeactivate:[CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AccessControlRoutingModule { }

