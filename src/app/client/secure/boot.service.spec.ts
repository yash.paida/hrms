/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { BootService } from './boot.service';

describe('Service: Boot', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [BootService]
    });
  });

  it('should ...', inject([BootService], (service: BootService) => {
    expect(service).toBeTruthy();
  }));
});
