import { NgModule } from '@angular/core';
import { RouterModule, Routes, Router } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SecureComponent } from './secure.component';
import { AccessGuard, CanDeactivateGuard } from 'src/app/core/access.guard';
import { AuthGuard } from 'src/app/core/auth.guard';

const routes: Routes = [
    {
        path: '',
        component: SecureComponent,
        canActivate: [AuthGuard],
        children: [
            {
                path: '',
                redirectTo: 'member',
                pathMatch: 'full'
            },
            {
                path: 'sample-page',
                loadChildren: './extra/sample-page/sample-page.module#SamplePageModule',
                canActivate: [AccessGuard], data: { is: ["Admin","User"] }
            },
            {
                path: 'member',
                loadChildren: './member/member.module#MemberModule',
                canActivate: [AccessGuard]
            },
            {
                path: 'accessControl',
                loadChildren: './accessControl/accessControl.module#AccessControlModule',
                canActivate: [AccessGuard]
            },
            {
                path: 'errors',
                loadChildren: './public/errors/errors.module#ErrorsModule'
            }
        ]
    }
];

@NgModule({
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class SecureRoutingModule {

    constructor(private router: Router) {
        this.router.errorHandler = (error: any) => {
            //console.log('Whats the errors:', error);
            if(error == 'Session Expired') this.router.navigate(['/auth']);
            this.router.navigate(['/errors']); // or redirect to default route
        }
    }
}
