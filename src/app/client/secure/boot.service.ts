import { Injectable } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';
import { Observable, of, concat, forkJoin, zip } from 'rxjs';
import { merge } from 'rxjs';
import { map, mergeAll, combineLatest } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BootService {

  public booted : boolean;

  constructor(private $: Utilities) { }

  public boot(): Observable<boolean> {

    if(this.booted) return of(true);
    return zip(
      //APIs for booting
      of(true) //Dummy API
    ).pipe(
      map(x => {
        console.log(x);
        this.booted = true;
        return true
      })
    );
  }

}