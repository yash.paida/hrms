import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import {SamplePageComponent} from './sample-page.component';
import { CanDeactivateGuard } from 'src/app/core/access.guard';

const routes: Routes = [
  {
    path: '',
    component: SamplePageComponent,
    canDeactivate:[CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SamplePageRoutingModule { }
