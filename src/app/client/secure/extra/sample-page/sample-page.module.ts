import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SamplePageRoutingModule } from './sample-page-routing.module';
import { SamplePageComponent } from './sample-page.component';
import { SharedModule } from '../../../../theme/shared/shared.module';
import { NgbDropdownModule } from '@ng-bootstrap/ng-bootstrap';
import { ChatUserListComponent } from './chat-user-list/chat-user-list.component';
import { ChatMsgComponent } from './chat-msg/chat-msg.component';
import { FriendComponent } from './chat-user-list/friend/friend.component';

@NgModule({
  imports: [
    CommonModule,
    SamplePageRoutingModule,
    SharedModule,
    NgbDropdownModule
  ],
  declarations: [
    SamplePageComponent,
    ChatUserListComponent,
    ChatMsgComponent,
    FriendComponent
  ]
})
export class SamplePageModule { }
