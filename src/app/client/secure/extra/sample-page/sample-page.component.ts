import { Component, OnInit, ViewChild, TemplateRef, DoCheck } from '@angular/core';
import { environment } from '../../../../../environments/environment';
import { animate, style, transition, trigger } from '@angular/animations';
import { Utilities } from 'src/app/core/utilities.service';
import { NgbDropdownConfig } from '@ng-bootstrap/ng-bootstrap';
import { ThemeConfig } from 'src/app/app-config';
import { Deserializable, Collection } from 'src/app/core/helper.service';
import { Application, permission, role } from 'src/app/services/authorization.api';
import { Observable } from 'rxjs';
import { CanComponentDeactivate } from 'src/app/core/access.guard';
import { AppPermissions, AppRoles } from 'src/app/core/access.service';
import { Permissions, Roles } from '../../secure.module';

@Component({
  selector: 'app-sample-page',
  templateUrl: './sample-page.component.html',
  styleUrls: ['./sample-page.component.scss'],
  providers: [NgbDropdownConfig],
  animations: [
    trigger('slideInOutLeft', [
      transition(':enter', [
        style({ transform: 'translateX(100%)' }),
        animate('300ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('300ms ease-in', style({ transform: 'translateX(100%)' }))
      ])
    ]),
    trigger('slideInOutRight', [
      transition(':enter', [
        style({ transform: 'translateX(-100%)' }),
        animate('300ms ease-in', style({ transform: 'translateX(0%)' }))
      ]),
      transition(':leave', [
        animate('300ms ease-in', style({ transform: 'translateX(-100%)' }))
      ])
    ])
  ]
})
export class SamplePageComponent implements OnInit, DoCheck, CanComponentDeactivate {


  @ViewChild('TopLeft', { static: true })
  private TopLeft: TemplateRef<any>

  @ViewChild('TopRight', { static: true })
  private TopRight: TemplateRef<any>

  @ViewChild('AppMenu', { static: true })
  private AppMenu: TemplateRef<any>

  @ViewChild('NavWidget', { static: true })
  private NavWidget: TemplateRef<any>


  public visibleUserList: boolean;
  public chatMessage: boolean;
  public friendId: boolean;
  public themeConfig: any;

  public topSearch: string = "Jay Swaminarayan";

  public AppCollection = new Collection<Application>("name", [], {
    "permissions": new Collection<permission>("name"),
    "roles": new Collection<role>("name"),
    "nest.roles": new Collection<role>("name"),
  });

    constructor(config: NgbDropdownConfig, public $: Utilities) {
    config.placement = 'bottom-right';
    this.visibleUserList = false;
    this.chatMessage = false;
    this.themeConfig = ThemeConfig.config;
  }

  ngOnInit() {

    this.$.UI.Customize.AppMenu = this.AppMenu;
    this.$.UI.Customize.TopLeft = this.TopLeft;
    this.$.UI.Customize.TopRight = this.TopRight;
    this.$.UI.Customize.NavWidget = this.NavWidget;

    this.$.Hotkey.register({
      combo: 'shift+g',
      handler: () => {
        console.log('Typed hotkey');
      },
      description: 'Sends a secret message to the console.'
    });

    this.$.UI.Device.Resize$.subscribe(size => {
      console.log(size, this.$.UI.Device);
    });

  }

  onChatToggle(friend_id) {
    this.friendId = friend_id;
    this.chatMessage = !this.chatMessage;
  }

  onSearchOpen(value: any) {
    console.log(value);
  }
  onSearchClose(value: any) {
    this.topSearch = "Swami";
    console.log('close', this.topSearch);
  }

  onSearch() {
    console.log('clicked');
  }

  ngDoCheck() {
    if (document.querySelector('body').classList.contains('theme-rtl')) {
      this.themeConfig['rtl-layout'] = true;
    } else {
      this.themeConfig['rtl-layout'] = false;
    }
  }

  public test()
  {
    console.log(this.$.Access.hasAccess('Can_Manage',{'Branch':'Hyderabad'}));
    console.log(this.$.Access.isRole('Admin',{'Branch':['Bangalore']}));
    console.log(this.$.Access.hasAccess(['Can_Manage_Package'],{'Branch':'Nagpur'}));
    console.log(this.$.Access.hasAccess('Can_Manage',{'Unit':'ICSE'}));
  }


  public cardRefresh($event) {
    console.log('refreshing...');
    this.test(); 

    setTimeout(() => {
      console.log('refreshed.');
      $event.next(true);
    }, 3000);
  }

  public cardDelete($event) {
    console.log('deleting...');
    setTimeout(() => {
      console.log('deleted.');
      $event.next(true);
    }, 5000);
  }


  doApps() {
    this.$.Api.Authorization.getAllApplications().subscribe(data => {
      console.log(data);
    },
      err => {
        this.$.Toast.error('Server Error');
      })
  }

  doDivs() {
    this.$.Api.Common.getDivisions().subscribe(data => {
      console.log(data);
    })
  }

  doData() {
    this.$.Api.Data.getTab().subscribe(data => {
      let datas = new Collection("id", data);
      console.log(datas);
    })
  }

  doDict() {

    console.log(this.$.Access.Applications);

    this.$.Api.Authorization.getAllApplications().subscribe(apps => {

      // let AppCollection = new Collection<Application>("name", apps, e => {
      //   let a = new Application();
      //   let p = new Collection<permission>("name", e.permissions);
      //   a = Object.assign({},e);
      //   a.permissions = p;
      //   return a;
      // });

      this.AppCollection.push(apps);


      console.log(this.AppCollection.values(), this.AppCollection.get("CRM").permissions.values());
      console.log(this.AppCollection.values(), this.AppCollection.get("CRM").roles.values());
      console.log(this.AppCollection.toArray());



    })
  }
  doDict2() {

    //this.AppCollection.get("Gurukul").roles.push([{ "id": "12as1df4ff", "name": "Final" }]);
    // this.$.Access.PermissionsChecked$.subscribe(state =>
    //   )
      this.$.Access.$Application.permissions.get('Can_Read').section = 'WOW'

    console.log(this.$.Access.$Application);
    console.log(this.$.Access.hasAccess(['Can_Log','Can_Manage']));


    

    // console.log(this.AppCollection.values(), this.AppCollection.get("CRM").permissions.values());
    // console.log(this.AppCollection.values(), this.AppCollection.get("CRM").roles.values());
    // console.log(this.AppCollection.toArray());

  }


  canDeactivate(): Observable<boolean> | boolean {

    return this.$.Access.DeactivateConfirm('Are you sure? Changes will be cancelled.');
    return true;
    // if (!this.isUpdating && this.personForm.dirty) {

    // }
    // return true;
  }

}

