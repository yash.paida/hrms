import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CanDeactivateGuard } from 'src/app/core/access.guard';
import { MemberComponent } from '../member/member.component';

const routes: Routes = [
  {
    path: "",
    component: MemberComponent,
    canDeactivate:[CanDeactivateGuard]
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MemberRoutingModule { }