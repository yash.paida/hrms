import {
  Component,
  OnInit,
  ViewChild,
  TemplateRef,
  OnDestroy
} from "@angular/core";
import { Utilities } from "src/app/core/utilities.service";
import { AppNavigation } from "./navigation";

@Component({
  selector: "app-secure",
  templateUrl: "./secure.component.html",
  styleUrls: ["./secure.component.scss"]
})
export class SecureComponent implements OnInit, OnDestroy {
  @ViewChild("UserSettings", { static: true })
  private UserSettings: TemplateRef<any>;

  constructor(public $: Utilities) {}

  ngOnDestroy(): void {}

  ngOnInit() {
    this.$.UI.Customize.UserSettings = this.UserSettings;

    this.$.Access.Bootstrapped$.subscribe(state => {
      this.$.Api.Common.getDivisions().subscribe(divs =>
        console.log("secure", divs)
      );
    });

    this.setNavigation();

    this.$.UI.Navigation.NavigationItems = [
      {
        id: "ac",
        title: "Access Control",
        type: "collapse",
        icon: "feather icon-sliders",
        parent: "navigation",
        children: [
          {
            id: "roles",
            title: "Roles",
            type: "item",
            classes: "nav-item",
            url: "/accessControl/roles"
          }
        ]
      },
      {
        id: "member",
        title: "Member",
        type: "item",
        icon: "feather icon-user",
        url: "/member",
        classes: "nav-item"
      }
    ];

    this.$.UI.Navigation.render();

    /** Access Controls */
    console.log(this.$.Access.hasAccess("Can_Log"));
  }

  setNavigation() {
    this.$.UI.Navigation.NavigationItems = AppNavigation;
    this.$.UI.Navigation.render();

    //console.log(this.$.UI.Navigation.getById('sample-page'));
    //console.log(this.$.UI.Navigation.getByUrl('/sample-page'));
  }
}
