import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { OidcComponent } from './oidc.component';
import { ForbiddenComponent } from './forbidden/forbidden.component';

const routes: Routes = [
    { path: '', component: OidcComponent },
    { path: 'unauthorized', component: ForbiddenComponent },
    { path: 'forbidden', component: ForbiddenComponent },
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class OidcRoutingModule {}
