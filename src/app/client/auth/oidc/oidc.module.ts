import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { OidcComponent } from './oidc.component';
import { OidcRoutingModule } from './oidc-routing.module';
import { SharedModule } from 'src/app/theme/shared/shared.module';
import { ForbiddenComponent } from './forbidden/forbidden.component';

@NgModule({
  imports: [
    CommonModule,
    OidcRoutingModule,
    SharedModule
  ],
  declarations: [OidcComponent, ForbiddenComponent]
})
export class OidcModule { }