import { Component, OnInit } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';

@Component({
  selector: 'app-forbidden',
  templateUrl: './forbidden.component.html',
  styleUrls: ['./forbidden.component.scss']
})
export class ForbiddenComponent implements OnInit {
  public params = {};
  constructor(public $ : Utilities) { 
    this.params = this.$.Helper.QueryParams;
  }

  ngOnInit() {
  }

}
