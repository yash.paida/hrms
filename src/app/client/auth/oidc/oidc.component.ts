import { Component, OnInit } from '@angular/core';
import { Utilities } from 'src/app/core/utilities.service';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-oidc',
  templateUrl: './oidc.component.html',
  styleUrls: ['./oidc.component.scss']
})
export class OidcComponent implements OnInit {

  public Message: string = "Loading..."

  constructor(public $: Utilities, private route: ActivatedRoute) { }

  ngOnDestroy(): void {
    //Called once, before the instance is destroyed.
    //Add 'implements OnDestroy' to the class.
    this.$.UI.Loader = false;
  }

  ngOnInit() {
    //console.log('Initializing /Auth');
    //this.route.
    if (window.location.hash) {
      const params = this.$.Helper.QueryParams
      this.Redirect(params);
    }
    else {
      this.route.queryParams.subscribe(params => {
        this.Redirect(params);
      });
    }

  }


  private Redirect(params) {
    this.$.UI.Loader = true;
    if (params['error'])
      this.$.Router.navigate(['/auth/forbidden'], { queryParams: params });
    if (!params['state']) {
      let toast = this.$.Toast.info("Redirecting...", null, { "positionClass": "toast-top-right", "timeOut": 10000, "progressAnimation": "increasing" }).toastId;
      this.Message = "Redirecting to " + (!this.$.Auth.isLoggedIn() ? 'Login' : this.$.Auth.getRedirectUrl());
      this.$.Auth.Authorize().subscribe(auth => {
        //console.log('/Auth Alive Value:', auth);
        this.$.Toast.remove(toast);
        this.$.UI.Loader = false;
        this.navigate();
      }, err => {
        //console.log(err);
        if (err == 'Login Failed') this.$.Auth.login();
        this.Message = err;
        this.$.Toast.remove(toast);
        this.$.UI.Loader = false;
      });
    }
  }

  navigate() {
    setTimeout(() => {
      //console.log('Navigating to:', this.$.Auth.getRedirectUrl());
      this.$.Router.navigate([this.$.Auth.getRedirectUrl()]);
    }, 25)
  }

}
