import { ApiClient, CacheStrategies } from '../core/api';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from 'src/environments/environment';
import { Deserializable, Collection } from '../core/helper.service';
import { of, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { AuthService } from '../core/auth.service';

export class AuthorizationService extends ApiClient {

    public Apps = new Collection<Application>("name", [], {
        "permissions": new Collection<permission>("name"),
        "roles": new Collection<role>("name"),
        "nest.roles": new Collection<role>("name"), //just testing not real
      });
    

    constructor(_http: HttpClient, _cache: LocalStorage) {
        //Call to the Parent Class's constructor.
        super("auth",environment.api.authorization.url, _http, _cache);
    }


    getAllApplications() : Observable<Application[]> {
        return this.get$('/Applications', null, CacheStrategies.CacheFirst).pipe(
            map(apps => <Application[]>apps)
        )
    }

    getUser(id : string) : Observable<userApplication> {
        return this.get$('/Users?Id=' + id, null, CacheStrategies.Freshness).pipe(
            map(apps => <userApplication>apps)
        )
    }

    clearUser(id : string) : Observable<boolean> {
        return this.clearCache$('/Users?Id=' + id)
    }


}


/* Models as Interfaces*/

export interface Application {
    id: string;
    name: string;
    description?: string;
    permissions?: Collection<permission>;
    roles?: Collection<role>;
    scopes?: [];
}

export interface permission {
    name: string;
    description?: string;
    section?: string;
    module?: string;
    scopes?: string[];
}

export interface role {
    id: string;
    name: string;
    description?: string;
    permissions?: string[];
}

export interface userApplication {
    applicationId: string;
    userRoles?: Collection<userRole>;
}

export interface userRole {
    name: string;
    permissions?: string[];
    scopes?: userScope
}

export interface userScope {
    [key: string] : string | string[];
}