import { ApiClient, CacheStrategies } from '../core/api';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from 'src/environments/environment';

export class CommonService extends ApiClient {

    constructor(_http: HttpClient, _cache: LocalStorage) {
        //Call to the Parent Class's constructor.
        super("common",`${environment.api.common.url}`, _http, _cache);
    }

    getDivisions() {
        return this.get$('/getDivisions', null, CacheStrategies.Performance)
    }

}