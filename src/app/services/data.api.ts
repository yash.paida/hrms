import { ApiClient, CacheStrategies } from '../core/api';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { environment } from 'src/environments/environment';

export class DataApiService extends ApiClient {

    constructor(_http: HttpClient, _cache: LocalStorage) {
        //Call to the Parent Class's constructor.
        super("data",`${environment.api.gbase.url}`, _http, _cache);
    }

    getTab() {
        return this.get$('/data', null, CacheStrategies.NoCache)
    }

}