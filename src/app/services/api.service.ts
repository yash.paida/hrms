import { Injectable } from '@angular/core';
import { AuthorizationService } from './authorization.api';
import { HttpClient } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { CommonService } from './common.api';
import { DataApiService } from './data.api';
import { ApiClient } from '../core/api';

@Injectable()
export class ApiService {
    constructor(private _http: HttpClient, private _cache: LocalStorage) { }

    //List All API Services created here with the short name you desire.
    public http = new ApiClient("","",this._http,this._cache);
    public Common = new CommonService(this._http, this._cache);
    public Authorization = new AuthorizationService(this._http, this._cache);
    public Data = new DataApiService(this._http, this._cache);

}