import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule, APP_INITIALIZER } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { SharedModule } from './theme/shared/shared.module';
import { AppComponent } from './app.component';
import { AdminComponent } from './theme/layout/admin/admin.component';
import { AuthComponent } from './theme/layout/auth/auth.component';
import { NavigationComponent } from './theme/layout/admin/navigation/navigation.component';
import { NavLogoComponent } from './theme/layout/admin/navigation/nav-logo/nav-logo.component';
import { NavContentComponent } from './theme/layout/admin/navigation/nav-content/nav-content.component';
import { NavigationService } from './theme/layout/admin/navigation/navigation.service';
import { NavGroupComponent } from './theme/layout/admin/navigation/nav-content/nav-group/nav-group.component';
import { NavCollapseComponent } from './theme/layout/admin/navigation/nav-content/nav-collapse/nav-collapse.component';
import { NavItemComponent } from './theme/layout/admin/navigation/nav-content/nav-item/nav-item.component';
import { NavBarComponent } from './theme/layout/admin/nav-bar/nav-bar.component';
import { ToggleFullScreenDirective } from './theme/shared/full-screen/toggle-full-screen';
import { NgbButtonsModule, NgbDropdownModule, NgbTabsetModule, NgbTooltipModule } from '@ng-bootstrap/ng-bootstrap';
import { NavLeftComponent } from './theme/layout/admin/nav-bar/nav-left/nav-left.component';
import { NavRightComponent } from './theme/layout/admin/nav-bar/nav-right/nav-right.component';
import { ConfigurationComponent } from './theme/layout/admin/configuration/configuration.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';
import { HttpClientModule } from '@angular/common/http';

import { NgxWebstorageModule } from "ngx-webstorage";
import { NgxHotkeysModule } from "@balticcode/ngx-hotkeys";
import { ToastrModule } from "ngx-toastr";

import { AuthModule, ConfigResult, AuthWellKnownEndpoints, OidcConfigService, OidcSecurityService, OpenIdConfiguration } from 'angular-auth-oidc-client';
import { AuthGuard } from './core/auth.guard';
import { AuthService } from './core/auth.service';
import { Utilities } from './core/utilities.service';
import { HelperService } from './core/helper.service';
import { localStorageProviders } from '@ngx-pwa/local-storage';
import { MomentModule } from "ngx-moment";
import { ApiService } from './services/api.service';
import { CommonUIService, DeviceResizeService } from './core/ui.service';
import { NgxOneSignalModule } from 'ngx-onesignal';


export function loadConfig(oidcConfigService: OidcConfigService) {
  return () =>
    oidcConfigService.load('config.json');
}


@NgModule({
  declarations: [
    AppComponent,
    AdminComponent,
    AuthComponent,
    NavigationComponent,
    NavLogoComponent,
    NavContentComponent,
    NavGroupComponent,
    NavCollapseComponent,
    NavItemComponent,
    NavBarComponent,
    ToggleFullScreenDirective,
    NavLeftComponent,
    NavRightComponent,
    ConfigurationComponent
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    SharedModule,
    NgbDropdownModule,
    NgbTooltipModule,
    NgbButtonsModule,
    NgbTabsetModule,
    // NgxOneSignalModule.forRoot({
    //   appId: environment.OneSignal.appId,
    //   allowLocalhostAsSecureOrigin: true,
    //   autoRegister: true,
    //   notifyButton: {
    //     enabled: true,
    //   },
    // }),
    // ServiceWorkerModule.register('OneSignalSDKWorker.js', { enabled: environment.production }),
    AuthModule.forRoot(),
    NgxWebstorageModule.forRoot({ prefix: environment.app.prefix, separator: '.', caseSensitive: true }),
    NgxHotkeysModule.forRoot({ cheatSheetCloseEsc: true }),
    MomentModule.forRoot(),
    ToastrModule.forRoot({
      "timeOut": 3000,
      "positionClass": 'toast-bottom-center',
      "preventDuplicates": true,
      "progressAnimation": 'decreasing',
      "tapToDismiss": true,
      "progressBar": true,
    }),
    NgxOneSignalModule.forRoot({ appId: environment.OneSignal.appId })

  ],
  providers: [
    localStorageProviders({ prefix: environment.app.prefix }),
    NavigationService,
    OidcConfigService,
    AuthService,
    ApiService,
    AuthGuard,
    Utilities,
    HelperService,
    CommonUIService,
    DeviceResizeService

    // {
    //   provide: APP_INITIALIZER,
    //   useFactory: loadConfig,
    //   deps: [OidcConfigService],
    //   multi: true,
    // }
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
  constructor(public _OAuthService: OidcSecurityService, public oidcConfigService: OidcConfigService, private _helper: HelperService) {
    // this.oidcConfigService.onConfigurationLoaded.subscribe((configResult: ConfigResult) => {
    //   console.log(configResult);
    // });
    const config: OpenIdConfiguration = environment.OAuth.config;
    let awkes = JSON.stringify(environment.OAuth.authWellKnownEndpoints);
    awkes = awkes.replace(/{stsServer}/gm, environment.OAuth.config.stsServer);
    awkes = awkes.replace(/{client_id}/gm, environment.OAuth.config.client_id);
    const authWellKnownEndpoints: AuthWellKnownEndpoints = JSON.parse(awkes);

    this._helper.Connection$.subscribe(net => {
      if (net) {
        if (!this._OAuthService.moduleSetup) {
          this._OAuthService.setupModule(config, authWellKnownEndpoints);
        }
      }
    });
    //console.log('AppModule: App Starting');
  }
}
