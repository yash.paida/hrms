import { Component, Injectable } from "@angular/core";
import { ActivatedRoute, Router } from "@angular/router";
//import { Service } from "./api";
import { AuthService } from "./auth.service";
//import { CommonDataService } from "../_common/common.data";
import { LocalStorage } from "@ngx-pwa/local-storage";
import { environment } from '../../environments/environment';
// import { MatSnackBar } from "@angular/material";
import { NgxHotkeysService } from "@balticcode/ngx-hotkeys";
import { HelperService } from "./helper.service";
import { LocalStorageService, SessionStorageService } from 'ngx-webstorage';
import { NavigationService } from '../theme/layout/admin/navigation/navigation.service';
import { Title } from '@angular/platform-browser';
import { ApiService } from '../services/api.service';
import { CommonUIService } from './ui.service';
import { ToastrService } from 'ngx-toastr';
import { AccessService, AppPermissions, AppRoles, AppScopes } from './access.service';
import { PushService } from './push.service';
import { BootService } from '../client/secure/boot.service';
//import { PushService } from "./push";
//import { CommonUIService } from "../_common/common.ui";


@Injectable()
export class Utilities {
    public Router = this._Router;
    public CurrentRoute = this._Route;
    public Api = this._Api;
    public Auth = this._Auth;
    public Access = this._Access;
    public Cache = this._Cache;
    public Local = this._Local;
    public Session = this._Session;
    public Environment = environment;
    //public SnackBar = this._SnackBar;
    public Hotkey = this._hotkeysService;
    //public Data = this._Data;
    public UI = this._UI;
    public Helper = this._Helper;
    public Push = this._Push;
    public Toast = this._Toast;
    
    public has(Permissions : AppPermissions | AppPermissions[], Scopes?: AppScopes) : boolean {
        return this._Access.hasAccess(Permissions, Scopes);
    }
    public is(Roles : AppRoles | AppRoles[], Scopes?: AppScopes) : boolean {
        return this._Access.isRole(Roles, Scopes);
    }

    get online() :boolean {
        return this._Helper.isOnline;
    }

    constructor(
        private _Api: ApiService,
        private _Auth: AuthService,
        private _Access: AccessService,
        private _Cache : LocalStorage,
        private _Local : LocalStorageService,
        private _Session : SessionStorageService,
        private _Router: Router,
        private _Route : ActivatedRoute,
        //private _SnackBar: MatSnackBar,
        private _hotkeysService: NgxHotkeysService,
        //private _Data : CommonDataService,
        private _UI : CommonUIService,
        private _Helper : HelperService,
        private _Push : PushService,
        private _Toast : ToastrService
    ) {}

    OnInit(): void {
        //Called at the AppComponent ngOnInit()
        this.Helper.OnInit();
        this.UI.OnInit();
        this.Auth.OnInit();
        this.Push.OnInit();
        this._UI.PageTitle = 'Welcome';

    }

    OnDestry() : void {
    
    }


    // public has(AccessPermission : string) : boolean {
    //     return this._Auth.hasAccess(AccessPermission);
    // }

}
