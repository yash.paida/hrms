import { Injectable, TemplateRef } from "@angular/core";
import { BehaviorSubject, Observable, Subscription, fromEvent, Subscriber, of } from "rxjs";
import { Title } from '@angular/platform-browser';
import { environment } from 'src/environments/environment';
import { NavigationService } from '../theme/layout/admin/navigation/navigation.service';
import { share } from 'rxjs/operators';
export enum MediaSizes { xs = 576, sm = 786, md = 992, lg = 1200, xl = 100000 }

@Injectable()
export class CommonUIService {
  public Loader: boolean = false;
  public Customize: CustomizeUI = new CustomizeUI();
  public Navigation = this._nav;
  public Device = this._resize;

  private _pageTitle: string;
  get PageTitle(): string { return this._pageTitle; }
  set PageTitle(title: string) { this._pageTitle = title; this._Title.setTitle(title + ' | ' + environment.app.name); }

  public showBreadcrumb: boolean;

  constructor(private _Title: Title, private _nav: NavigationService, private _resize: DeviceResizeService) {

  }


  public OnInit(): void {

    //  Called at AppComponent.ngOnInit() :: (Not a Native OnInit function)
    //  As ngOnInit doesn't execute in Services/Injectable Class
    this._resize.OnInit();
  }



}

class CustomizeUI {
  AppMenu: TemplateRef<any>;
  TopLeft: TemplateRef<any>;
  TopRight: TemplateRef<any>;
  NavWidget: TemplateRef<any>;
  UserSettings: TemplateRef<any>;
}


@Injectable()
export class DeviceResizeService {
  public size: number = MediaSizes.xs;
  public height: number = window.innerHeight;
  public width: number = window.innerWidth;
  private WindowResize$: Observable<Event>;
  public Resize$: Observable<MediaSizes>;
  private ResizeSubscriber: Subscriber<MediaSizes> = new Subscriber<MediaSizes>();

  constructor() {
    this.WindowResize$ = fromEvent(window, 'resize');
    this.Resize$ = new Observable<MediaSizes>((observer: Subscriber<MediaSizes>) => this.ResizeSubscriber = observer).pipe(share());
  }

  OnInit() {
    this.calcSizes();
    this.WindowResize$.subscribe(evt => {
      this.calcSizes();
    })
  }

  calcSizes() {
    this.height = window.innerHeight;
      this.width = window.innerWidth;
      let w = this.width;
      let s = this.size;
      switch (true) {
        case (w < 576): this.size = MediaSizes.xs; break;
        case (w < 768): this.size = MediaSizes.sm; break;
        case (w < 992): this.size = MediaSizes.md; break;
        case (w < 1200): this.size = MediaSizes.lg; break;
        default: this.size = MediaSizes.xl; break;
      }
      if (this.size != s) {
        //console.log(w);
        this.ResizeSubscriber.next(this.size);
      }
  }

}