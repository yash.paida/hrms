import { Injectable } from '@angular/core';
import { Collection, HelperService } from './helper.service';
import { Application, permission, role, userApplication, userRole, userScope } from '../services/authorization.api';
import { ApiService } from '../services/api.service';
import { environment } from 'src/environments/environment';
import { AuthService } from './auth.service';
import { union, intersection, toUpper } from "lodash";
import { map, concatMap, mergeMap, catchError, retry } from 'rxjs/operators';
import { of, Observable, Subject } from 'rxjs';
import { ToastrService } from 'ngx-toastr';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { Router } from '@angular/router';
import { Permissions, Roles, Scopes } from '../client/secure/secure.module';
import { PushService } from './push.service';

export type AppRoles = keyof typeof Roles;
export type AppPermissions = keyof typeof Permissions;
export type AppScopeKeys = keyof typeof Scopes;
export type AppScopes = { [key in AppScopeKeys]?: string | string[] };
type scopedPermissions = { [key in AppPermissions]?: { scopes: AppScopes } };
type scopedRoles = { [key in AppRoles]?: { scopes: AppScopes } };

@Injectable({
  providedIn: 'root'
})
export class AccessService {


  public $Application: Application;
  public $UserApplication: userApplication;
  public Permissions: scopedPermissions = {};
  public Roles: scopedRoles = {};
  public Scopes: userScope = {};
  public _boot: boolean;

  public PermissionsSynced$ = new Subject<userApplication>();
  public Bootstrapped$ = new Subject<boolean>();
  


  private readonly SuperAdminRole = 'Super Admin';
  public isSuperAdmin: boolean;

  //The complete Collection Relations to be defiened this way.
  public Applications = new Collection<Application>("id", [], {
    "permissions": new Collection<permission>("name"),
    "roles": new Collection<role>("name")
  });
  //The complete Collection Relations to be defiened this way.
  public UserApplications = new Collection<userApplication>("applicationId", [], {
    "userRoles": new Collection<userRole>("name")
  });

  constructor(private _Api: ApiService, private _Router: Router, private _Auth: AuthService, private _Push: PushService, private _Helper: HelperService, private _Toast: ToastrService, private _cache: LocalStorage) {

    this._Auth.onAuthRenew$.subscribe(auth => {
      this._Api.Authorization.SetHeaders({ "Authorization": this._Auth.AccessToken });
    }, err => {
      if (err == 'Login Failed') {
        this._boot = false;
        let toast = this._Toast.warning("Reconnecting...", null, { "positionClass": "toast-top-center", "timeOut": 5000, "progressAnimation": "increasing" }).toastId;
        setTimeout(() => window.location.reload(true), 3000);
      }
      else this._Router.navigate(['/auth']);
    });


    this.Bootstrapped$.subscribe(state => {
      if (this._Helper.isOnline)
        this._Push.setUniqueID(this._Auth.User.sub, this._Auth.User.preferred_username, this._Auth.User.email).subscribe(id => { console.log(id) });
    });

    this._Auth.beforeLogout$.subscribe(state => {
      this._Api.Authorization.clearUser(this._Auth.User.sub).subscribe();
      this.Permissions = {};
      this.Roles = {};
      this.Scopes = {};
      this.$UserApplication.userRoles.clear();
      this.UserApplications.clear();
      this._Api.Authorization.ResetHeaders();
      this._boot = false;
      if (this._Helper.isOnline) {
        this._Push.unsetUniqueID().subscribe(id => {
          //console.log(id)
          this._Auth.LogoutSubscriber.complete()
        },
          err => { this._Auth.LogoutSubscriber.complete() },
          () => this._Auth.LogoutSubscriber.complete()
        );
      }
      else {
        this._Auth.LogoutSubscriber.complete()
      }
    });





  }

  public syncPermissions(): Observable<boolean> {

    if (this._boot) return of(true);
    return this._Auth.isAlive().pipe(
      map(user => {
        console.log(user);
        //const token = "Bearer " + this._Auth._OAuthService.getToken();
        this._Auth.onAuthRenew$.next(this._Auth.AccessToken);
        this._Api.Authorization.SetHeaders(Object.assign({},
          environment.api.authorization.headers, { "AppId": environment.OAuth.config.client_id, "Authorization": this._Auth.AccessToken }));
        return true
      }),
      concatMap(done => {
        console.log(done);
        return this._Api.Authorization.getAllApplications()
      }),
      concatMap((apps: Application[]) => {
        console.log(apps);
        this.Applications.push(apps)
        this.$Application = this.Applications.get(environment.OAuth.config.client_id)
        return of(true);
      }),
      concatMap(done => {
        console.log(done);
        return this._Api.Authorization.getUser(this._Auth.User.sub)
      }),
      concatMap(userApps => {
        let ua = <userApplication>userApps;
        return of(this.SetPermissions(ua));
      }),
      concatMap(state => {
        console.log("Secure Boot: ", state);
        this._boot = true;
        this.Bootstrapped$.next(true);
        return of(true);
      }),
      catchError(err => {
        this._Router.navigate(['/errors'], { queryParams: { error: 'Access Denied', error_reason: 'You are not having sufficient permissions for this operation.' } })
        return of(false);
      })
    );

  }


  private SetPermissions(userApps: userApplication): boolean {
    //console.log(userApps);
    this.UserApplications.push(userApps["applications"]);
    this.$UserApplication = this.UserApplications.get(environment.OAuth.config.client_id);
    //console.log(this.$UserApplication);
    if (this._Auth.User.roles.indexOf(this.SuperAdminRole) != -1) {
      //console.log('isSuperAdmin');
      this.isSuperAdmin = true;
      return true;
    }
    else
      this.isSuperAdmin = false;
    //Smart Permissions Resolution Dict

    this.$Application.permissions.values().forEach(appPermission => {
      //console.log(appPermission);
      this.$UserApplication.userRoles.values().forEach(userRole => {
        //console.log(userRole);
        if (userRole.permissions.indexOf(appPermission.name) >= 0) {
          //console.log(appPermission.name, <AppRoles>userRole.name);
          if (!this.Roles[<AppRoles>userRole.name])
            this.Roles[<AppRoles>userRole.name] = { scopes: {} };
          //console.log(this.Roles[<AppRoles>userRole.name]);
          this.Roles[<AppRoles>userRole.name].scopes = userRole.scopes;
          if (!this.Permissions[<AppPermissions>appPermission.name])
            this.Permissions[<AppPermissions>appPermission.name] = { scopes: {} };
          appPermission.scopes.forEach(pScope => {
            //console.log(pScope);
            if (!this.Permissions[<AppPermissions>appPermission.name].scopes[<AppScopeKeys>pScope])
              this.Permissions[<AppPermissions>appPermission.name].scopes[<AppScopeKeys>pScope] = [];
            //console.log(userRole.scopes[pScope]);
            this.Permissions[<AppPermissions>appPermission.name].scopes[<AppScopeKeys>pScope] =
              union(this.Permissions[<AppPermissions>appPermission.name].scopes[<AppScopeKeys>pScope], userRole.scopes[pScope]);
          });
        }
      });
    });
    return true;
  }

  public hasAccess(Permissions: AppPermissions | AppPermissions[], scopes?: AppScopes): boolean {
    //return true;
    if (!this._boot) return false;
    if (this.isSuperAdmin) return true;
    let permissions = [];
    if (!(Permissions instanceof Array)) Permissions = [Permissions];
    if (Permissions.length == 0) return true;
    permissions = intersection(Permissions, Object.keys(this.Permissions));
    if (permissions.length > 0)
      if (scopes) return permissions.filter(p => { return this.resolvePermissions(p, scopes) }).length > 0
    return permissions.length > 0
  }

  public isRole(Roles: AppRoles | AppRoles[], scopes?: AppScopes): boolean {

    if (!this._boot) return false;
    if (this.isSuperAdmin) return true;
    let roles = [];
    if (!(Roles instanceof Array)) Roles = [Roles];
    if (Roles.length == 0) return true;
    roles = intersection(Roles, Object.keys(this.Roles));
    if (roles.length > 0)
      if (scopes) return roles.filter(r => { return this.resolveRoles(r, scopes) }).length > 0
    return roles.length > 0;
  }

  private resolvePermissions(permission: AppPermissions, scopes?: AppScopes): boolean {

    //'Can_Manage_Package',{'Branch':'Hyderabad','Unit':['CBSE','ICSE']}
    if (!scopes) return true;
    let expectedScopes = this.Permissions[permission].scopes;
    let scopeMatched: boolean;
    Object.keys(expectedScopes).some(scope => {
      if (scopes[scope]) {
        if (typeof scopes[scope] == 'string') scopes[scope] = [scopes[scope]];
        return scopeMatched = intersection(this.Permissions[permission].scopes[scope], scopes[scope]).length > 0
      }
    });
    if (scopeMatched) return true;
    //console.warn('Expected Scope or Value Not Found.');
    return false;
  }

  private resolveRoles(role: AppRoles, scopes?: AppScopes): boolean {

    //'Can_Manage_Package',{'Branch':'Hyderabad','Unit':['CBSE','ICSE']}

    if (!scopes) return true;
    let expectedScopes = this.Roles[role].scopes;
    let scopeMatched: boolean;
    Object.keys(expectedScopes).some(scope => {
      if (scopes[scope]) {
        if (typeof scopes[scope] == 'string') scopes[scope] = [scopes[scope]];
        return scopeMatched = intersection(this.Roles[role].scopes[scope], scopes[scope]).length > 0
      }
    });
    if (scopeMatched) return true;
    //console.warn('Expected Scope/Value Not Found.');
    return false;
  }


  public evalAccess(Permission: SmartAccess): Observable<boolean> {
    console.log(Permission);
    if (this.isSuperAdmin) return this.syncPermissions();
    if (Permission.allow) {
      if(Permission.allow()) return this.syncPermissions();
      else return of(false);
    }
    return this.syncPermissions().pipe(
      map(syncState => {
        console.log(syncState);
        if (syncState) {
          let hasAccess: boolean = false;
          let isRole: boolean = false;

          if (Permission.has != null) {
            //console.log(Permission.hasAccess);
            hasAccess = this.hasAccess(Permission.has, Permission.scopes)
          }

          if (Permission.is != null) {
            isRole = this.isRole(Permission.is, Permission.scopes);
            //console.log(Permission.isRole, isRole);
          }

          console.log(hasAccess || isRole || JSON.stringify(Permission) === JSON.stringify({}) )

          return hasAccess || isRole || JSON.stringify(Permission) === JSON.stringify({});
        }
        else return false
      })
    )
  }

  DeactivateConfirm(message?: string): Observable<boolean> {
    const confirmation = window.confirm(message || 'Is it OK?');

    return of(confirmation);
  };
}

export interface SmartAccess {
  has?: AppPermissions[],
  is?: AppRoles[],
  scopes?: AppScopes,
  allow?: () => boolean
}