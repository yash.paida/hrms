
import { fromEvent as observableFromEvent, of as observableOf, merge as observableMerge, Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import { Injectable } from "@angular/core";
//import { PageEvent } from "@angular/material";
import { set, get, has, clone, cloneDeep } from 'lodash';
import { ActivatedRoute } from '@angular/router';
import { PageEvent, MatPaginatorIntl } from '@angular/material';

function getWindow(): any {
  return window;
}




@Injectable()
export class HelperService {
  public Keys = Object.keys; //To Search in the JSON keys :: Native JS Object.keys
  public isOnline = navigator.onLine; //To check internet status :: Native JS navigator.onLine
  public Connection$: Observable<boolean>; //Trigger to check internet status :: Native JS navigator.onLine
  public _ = { set, get, clone };

  constructor(public thisRoute: ActivatedRoute) {

    //Creating a Trigger once the window state change
    this.Connection$ = observableMerge(
      observableOf(navigator.onLine),
      observableFromEvent(window, 'online').pipe(map(() => true)),
      observableFromEvent(window, 'offline').pipe(map(() => false)));

  }

  public OnInit(): void {

    //  Called at AppComponent.ngOnInit() :: (Not a Native ngOnInit function)
    //  As ngOnInit doesn't execute in Services/Injectable Class

    //Connection trigger subscriber
    this.Connection$.subscribe(state => {
      if (state) document.body.classList.remove("offline")
      else document.body.classList.add("offline")
      this.isOnline = state;
    });

  }

  /** Start writing your common functions here */

  public MyFoo(MyData: any[]) {
    return true;
  }

  public window(): any {
    return getWindow();
  }

  /**
   * Query2Json
   */
  public Query2Json(queryString: string) {

    //console.log(unescape(queryString));
    const queryStringPieces = queryString.split("&");
    const decodedQueryString = {};

    for (const piece of queryStringPieces) {
      let [key, value] = piece.split("=");
      value = value || "";
      if (has(decodedQueryString, key)) {
        const currentValueForKey = get(decodedQueryString, key);
        if (!Array.isArray(currentValueForKey)) {
          set(decodedQueryString, key, [currentValueForKey, decodeURI(unescape(value))]);
        } else {
          currentValueForKey.push(decodeURI(unescape(value)));
        }
      } else {
        set(decodedQueryString, key, decodeURI(unescape(value)));
      }
    }
    return decodedQueryString;
  }

  get QueryParams(): any {
    var urlParams = '';
    if (window.location.hash) urlParams = window.location.hash.substring(1)
    else urlParams = window.location.search.substring(1);
    if (urlParams != '') return this.Query2Json(urlParams);
    else return false;
  }


}



export interface Deserializable<T> {
  deserialize(input: any): T;
  // https://nehalist.io/working-with-models-in-angular/
}



/*

export class Dictionary<T> {
  private _indexKey: string;
  private count: number = 0;
  private _items: T[] = [];

  constructor(indexKey: string, items: T[] = []) {
    this._indexKey = indexKey;
    this.push(items);
  }

  public has(key: string): boolean {
    return this._items.hasOwnProperty(key);
  }

  public length() {
    return this.count;
  }

  public set(key: string, value: T) {
    if (!this._items.hasOwnProperty(key))
      this.count++;
    //console.log(this._p(key));
    this._items[key] = value;
  }


  public get(key: string): T {
    return this._items[key];
  }

  public remove(key: string): T {
    var val = this._items[key];
    delete this._items[key];
    this.count--;
    return val;
  }

  public keys(): string[] {
    var keySet: string[] = [];

    for (var prop in this._items) {
      if (this._items.hasOwnProperty(prop)) {
        keySet.push(prop);
      }
    }

    return keySet;
  }

  public values(): T[] {
    var _values: T[] = [];

    for (var prop in this._items) {
      if (this._items.hasOwnProperty(prop)) {
        _values.push(this._items[prop]);
      }
    }

    return _values;
  }


  public clear() {
    this._items = [];
    this.count = 0;
  }

  public filter(fn: (element, index, array) => boolean): T[] {
    let result: T[] = [];
    result = this.values().filter(fn);
    //result = this.items;
    return result;
  }

  public push(items: T[]): void {
    let keyColumn = this._indexKey;
    if (!Array.isArray(items)) return;
    items.forEach(element => {
      //console.log(element[keyColumn]);
      this.set(element[keyColumn], element);
    });
  }

  private _p(key: string): string { return isNaN(+key) ? key : '+' + key; }
  private _k(key: string): string { return isNaN(+key) ? key : key.substr(1); }


}
*/

export class Dictionary<T> {
  private _indexKey: string = "id";
  private relations: { [key: string]: Dictionary<any> };
  private count: number = 0;
  private _itemsArray: T[] = [];
  private _items: { [key: string]: T } = {};
  
  constructor(indexKey: string = "id", items: T[] = [], relations: { [key: string]: Dictionary<any> } = {}) {
    this._indexKey = indexKey;
    this.relations = relations;
    this.push(items);
  }

  public push(items: T[]): void {
    let keyColumn = this._indexKey;
    if (!Array.isArray(items)) return;

    let _items = cloneDeep(items);
    //console.log(_items);
    _items.forEach(element => {
      //console.log(element[keyColumn]);
      this.set(element[keyColumn], element);
      this._itemsArray.push(element);
    });
  }

  public set(key: string, value: any) {
    if (!this._items.hasOwnProperty(key))
      this.count++;
    //console.log(this._p(key));
    this._items[key] = this.transformer(value);
  }


  private transformer: (e: any) => T = (e: any) => {
    let ele: T;
    ele = Object.assign({}, e);
    let rels = Object.keys(this.relations);
    rels.forEach(r => {
      let prop = this.relations[r].clone();
      prop.clear();
      let val = get(ele, r, []);

      prop.push(Object.assign([], val));
      set(ele, r, prop.clone());
    });
    return ele;
  };


  public clone() {
    return new Dictionary<T>(this._indexKey, cloneDeep(this.values(), true), this.relations);
  }

  public clear() {
    this._items = {};
    this.count = 0;
  }

  public values():  { [key: string]: T } {
    var _values: { [key: string]: T } = {};
    
    _values = this._items;

    //return Object.values(this._items);
     return _values;
  }

  public array():  T[] {
    var _values: T[] = [];
    
    _values = this._itemsArray;

    //return Object.values(this._items);
     return _values;
  }

  private flatter: (e: T) => any = (e: T) => {
    let ele: any;
    ele = Object.assign({}, e);
    let rels = Object.keys(this.relations);
    rels.forEach(r => {
      let prop = this.relations[r].clone();
      prop.clear();
      let val = get(e, r, prop);
      prop = val.clone();
      set(ele, r, Object.assign({}, prop.values()));
    });
    return ele;
  };


  public get(key: string): T {
    return this._items[key];
  }

  public remove(key: string): T {
    var val = this._items[key];
    delete this._items[key];
    this.count--;
    return val;
  }

  public keys(): string[] {
    var keySet: string[] = Object.keys(this._items);
    return keySet;
  }

  public filter(fn: (element, index, array) => boolean): T[] {
    let result: T[] = [];
    Object.keys(this.values()).forEach(key => {
      result.push(this._items[key]);
    });
    //result = this.values().filter(fn);
    //result = this.items;
    return result;
  }




  public toArray() {
    var _values: any[] = [];
    var items = this.objCopy(this._items);
    for (var prop in items) {
      if (items.hasOwnProperty(prop)) {
        _values.push(this.flatter(items[prop]));
      }
    }

    return _values;
  }

  private objCopy(obj) {
    var clone = {};
    for (var i in obj) {
      if (obj[i] != null && typeof (obj[i]) == "object")
        clone[i] = this.objCopy(obj[i]);
      else
        clone[i] = obj[i];
    }
    return clone;
  }
}


export class Collection<T> {
  private _indexKey: string = "id";
  private relations: { [key: string]: Collection<any> };
  private count: number = 0;
  private _items: T[] = [];

  constructor(indexKey: string = "id", items: T[] = [], relations: { [key: string]: Collection<any> } = {}) {
    this._indexKey = indexKey;
    this.relations = relations;
    this.push(items);
  }

  private transformer: (e: any) => T = (e: any) => {
    let ele: T;
    ele = Object.assign({}, e);
    let rels = Object.keys(this.relations);
    rels.forEach(r => {
      let prop = this.relations[r].clone();
      prop.clear();
      let val = get(ele, r, []);

      prop.push(Object.assign([], val));
      set(ele, r, prop.clone());
    });
    return ele;
  };

  private flatter: (e: T) => any = (e: T) => {
    let ele: any;
    ele = Object.assign({}, e);
    let rels = Object.keys(this.relations);
    rels.forEach(r => {
      let prop = this.relations[r].clone();
      prop.clear();
      let val = get(e, r, prop);
      prop = val.clone();
      set(ele, r, Object.assign({}, prop.values()));
    });
    return ele;
  };

  public has(key: string): boolean {
    return this._items.hasOwnProperty(this._p(key));
  }

  public length() {
    return this.count;
  }

  public set(key: string, value: any) {
    if (!this._items.hasOwnProperty(this._p(key)))
      this.count++;
    //console.log(this._p(key));
    this._items[this._p(key)] = this.transformer(value);
  }


  public get(key: string): T {
    return this._items[this._p(key)];
  }

  public remove(key: string): T {
    var val = this._items[this._p(key)];
    delete this._items[this._p(key)];
    this.count--;
    return val;
  }

  public keys(): string[] {
    var keySet: string[] = [];

    for (var prop in this._items) {
      if (this._items.hasOwnProperty(prop)) {
        keySet.push(this._k(prop));
      }
    }

    return keySet;
  }

  public values(): T[] {
    var _values: T[] = [];
    for (var prop in this._items) {
      if (this._items.hasOwnProperty(prop)) {
        _values.push(this._items[prop]);
      }
    }

    //return Object.values(this._items);
     return _values;
  }

  public toArray() {
    var _values: any[] = [];
    var items = this.objCopy(this._items);
    for (var prop in items) {
      if (items.hasOwnProperty(prop)) {
        _values.push(this.flatter(items[prop]));
      }
    }

    return _values;
  }

  public clear() {
    this._items = [];
    this.count = 0;
  }

  private objCopy(obj) {
    var clone = {};
    for (var i in obj) {
      if (obj[i] != null && typeof (obj[i]) == "object")
        clone[i] = this.objCopy(obj[i]);
      else
        clone[i] = obj[i];
    }
    return clone;
  }

  public clone() {
    return new Collection<T>(this._indexKey, clone(this.values(), true), this.relations);
  }

  public filter(fn: (element, index, array) => boolean): T[] {
    let result: T[] = [];
    result = this.values().filter(fn);
    //result = this.items;
    return result;
  }

  public push(items: T[]): void {
    let keyColumn = this._indexKey;
    if (!Array.isArray(items)) return;
    items.forEach(element => {
      //console.log(element[keyColumn]);
      this.set(element[keyColumn], element);
    });
  }

  // private _p(key: string): string { return isNaN(+key) ? key : '+' + key; }
  // private _k(key: string): string { return isNaN(+key) ? key : key.substr(1); }
  private _p(key: string): string { return key; }
  private _k(key: string): string { return key; }


}

/** for Angular Material */

export class CustomMatPaginatorIntl extends MatPaginatorIntl {
  itemsPerPageLabel = 'Show:'; 
}

export class ViewModel<T> {
  private _VM: T[] = [];
  values: T[] = [];
  pageIndex = 0;
  length = 1000;
  size = 25;
  options = [10, 25, 50, 100];

  //
  updateValues(): void {
    let returnArray: T[] = [];
    if (this._VM.length) {
      for (var index = this.pageIndex * this.size; index < (this.pageIndex + 1) * this.size; index++) {
        var element = this._VM[index];
        if (element) returnArray.push(element);
      }
    }
    //console.log(returnArray);
    this.values = returnArray;
  }

  get VM(): T[] {
    return this._VM;
  }
  set VM(data: T[]) {
    this.pageIndex = 0;
    this.length = data.length;
    this._VM = data;
    this.updateValues();
  }
  change($e: PageEvent) {
    //console.log($e);
    this.pageIndex = $e.pageIndex;
    this.size = $e.pageSize;
    this.updateValues();
  }

  clear() {

  }

}
