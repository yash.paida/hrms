import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, CanDeactivate, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { AccessService } from './access.service';
import { AuthService } from './auth.service';
import { AuthGuard } from './auth.guard';
import { map, concatMap } from 'rxjs/operators';
import { BootService } from '../client/secure/boot.service';

@Injectable()
export class AccessGuard implements CanActivate {

    constructor(private router: Router, private _Access: AccessService, private _Auth: AuthService, private _AuthGuard: AuthGuard, private _boot : BootService) { }
    canActivate(
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot,
    ): Observable<boolean> | Promise<boolean> | boolean {
        //console.log('Bypassing Auth'); return true;
        this._Auth.setRedirectUrl(state.url.indexOf('/errors') >= 0 ? '/' : state.url);
        //let AuthState = this._Access.evalAccess(route.data);
   
        //console.log(route.data);
        return this._Access.evalAccess(route.data).pipe(
            concatMap(
                AuthState => {
                    console.log('h1', AuthState);
                    if (AuthState) return of(true);
                    else {
                        this.router.navigate(['/errors'], { queryParams: { error: 'Access Denied', error_reason: 'You are not having sufficient permissions for this operation.' } })
                        return of(false);
                    }
                }),
            concatMap(state => this._boot.boot()),
            concatMap(stat => this._AuthGuard.canActivate(route, state))
        );
        //console.log(AuthState, route.data);
    }

}

export interface CanComponentDeactivate {
    canDeactivate: () => Observable<boolean> | Promise<boolean> | boolean;
}

@Injectable({
    providedIn: 'root',
})
export class CanDeactivateGuard implements CanDeactivate<CanComponentDeactivate> {
    canDeactivate(
        component: CanComponentDeactivate,
        route: ActivatedRouteSnapshot,
        state: RouterStateSnapshot
    ): Observable<boolean> | Promise<boolean> | boolean {
        return component.canDeactivate ? component.canDeactivate() : true;
        //return true;
    }
}
