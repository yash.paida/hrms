import { Injectable } from '@angular/core';
import { Router, Route, CanActivate, CanLoad, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';

import { OidcSecurityService } from 'angular-auth-oidc-client';
import { AuthService } from './auth.service';
import { HelperService } from './helper.service';

@Injectable()
export class AuthGuard implements CanActivate, CanLoad {
    constructor(private router: Router, private _Auth: AuthService, private _helper: HelperService) { }

    canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot): Observable<boolean> {
        //return of(true);
        this._Auth.setRedirectUrl(state.url.indexOf('/errors') >= 0 ? '/' : state.url);
        //console.log(state.url);
        return this.checkUser();
    }

    canLoad(state: Route): Observable<boolean> {
        return this.checkUser();
    }

    private checkUser(): Observable<boolean> {
        if (!this._Auth.isLoggedIn()) {
            this.router.navigate(['/auth']);
            return of(false);
        }
        return this._Auth.isAlive().pipe(
            map((User: any) => {
                //console.log('/Guard ', User);
                return true;
            }),
            catchError((err) => {
                //console.log("error Guard Error");
                //console.log("Token Dead or Not Found");
                if (this._helper.isOnline) this.router.navigate(['/auth']);
                else this.router.navigate(['/auth/unauthorized']);
                return of(false);
            })
        );
    }
}