import { Injectable } from '@angular/core';
import { Observable, Subscriber, timer, throwError, of, Subject } from 'rxjs';
import { map, share, catchError, takeUntil, first, finalize, concatMap } from 'rxjs/operators';
import { OidcSecurityService } from 'angular-auth-oidc-client';
import { LocalStorageService } from 'ngx-webstorage';
import { Router } from '@angular/router';
import { HelperService } from './helper.service';
import { environment } from 'src/environments/environment';
import { CommonUIService } from './ui.service';

@Injectable()
export class AuthService {
    private _isLoggedIn = false;

    public AuthData: any;
    public User: AppUser;

    public onAlive$: Observable<AppUser>;
    public AliveSubscriber: Subscriber<AppUser> = new Subscriber<AppUser>();
    public beforeLogout$: Observable<boolean>;
    public LogoutSubscriber: Subscriber<boolean> = new Subscriber<boolean>();
    public onAuthRenew$: Subject<string> = new Subject<string>();
    private _silentRenewInit: any;
    private _silentRenewStart: any;
    private _silentRenewStop: any;
    private _accessToken: string;
    get AccessToken(): string {
        return this._accessToken;
    }


    constructor(public _OAuthService: OidcSecurityService, private _Local: LocalStorageService, private _helper: HelperService, private _ui: CommonUIService) {

        this.onAlive$ = new Observable<AppUser>((observer: Subscriber<AppUser>) => this.AliveSubscriber = observer).pipe(share());
        this.beforeLogout$ = new Observable<boolean>((observer: Subscriber<boolean>) => this.LogoutSubscriber = observer).pipe(share(),
            //takeUntil(timer(5000)),
            finalize(() => {
                this._OAuthService.logoff();
                this._ui.Loader = false;
            }),
            catchError((e, o) => {
                this._OAuthService.logoff();
                this._ui.Loader = false;
                return of(false);
            })
        );
        this.User = {};
        //this.User.picture = 'https://img.icons8.com/ios-filled/100/4a90e2/test-account.png'; //Default User Picture.
        
        this.setAccessFromStorage();

        //this._OAuthService.onAuthorizationResult.subscribe(r => console.log('AuthResult:', r));
        //this._OAuthService.onCheckSessionChanged.subscribe(r => console.log('AuthSession:', r));


        if (this._OAuthService.moduleSetup) {
            this.doCodeGrant();
        } else {
            this._OAuthService.onModuleSetup.subscribe(() => {
                this.doCodeGrant();
            });
        }



    }

    OnInit() {



        this._OAuthService.onAuthorizationResult.subscribe(Auth => {
            //console.log(Auth);
            this.setAccessFromStorage();
            this.onAuthRenew$.next(this._accessToken);
        });

        this._helper.Connection$.pipe(
            map(net => {
                if (net && this._OAuthService.moduleSetup) {
                    // console.log('Refresh force: ', !this._OAuthService.moduleSetup)                    
                    // else return 
                    this._OAuthService.getIsAuthorized().subscribe(authState => {
                        if (!authState) this.onAuthRenew$.error('Renewal Failed');
                    })

                    let x = JSON.parse(localStorage.getItem('authorizationResult_' + environment.OAuth.config.client_id));
                    //console.log('Expires: ', x['expires_in']);
                    if (x) return true;
                    else false;
                }
                else return false
            })
        ).subscribe(net => {
            //console.log(net);
            if (net) {
                let x = JSON.parse(localStorage.getItem('authorizationResult_' + environment.OAuth.config.client_id));
                if (x && environment.OAuth.config.silent_renew) {
                    if (!this._silentRenewStop) {
                        //console.log(x['expires_in']);
                        this._silentRenewInit = setTimeout(() => {
                            //console.log('SR Installed');
                            this._OAuthService.startCheckingSilentRenew();
                            this._silentRenewStop = setInterval(() => {
                                //console.log('SR Stops');
                                this._OAuthService.stopCheckingSilentRenew();
                                this._silentRenewStart = setTimeout(() => {
                                    //console.log('SR Starts');
                                    if (this._helper.isOnline) {
                                        this._OAuthService.startCheckingSilentRenew();
                                        clearTimeout(this._silentRenewStart);
                                        this._silentRenewStart = null;
                                    }
                                    else {
                                        this._OAuthService.stopCheckingSilentRenew();
                                        clearTimeout(this._silentRenewStart);
                                        this._silentRenewStart = null;
                                        clearInterval(this._silentRenewStop);
                                        this._silentRenewStop = null;
                                    }
                                }, x['expires_in'] * 1000)
                            }, (x['expires_in'] - 10) * 1000);
                            clearTimeout(this._silentRenewInit);
                            this._silentRenewInit = null;
                        }, 5000)
                    }
                }
                // this._OAuthService.getIsAuthorized().subscribe(
                //     authState => {
                //         this._isLoggedIn = authState;
                //         console.log('Init: ', authState);
                //     }
                // );
            }
            else {
                //console.log('Stops: SR');
                clearTimeout(this._silentRenewStart);
                this._silentRenewStart = null;
                clearInterval(this._silentRenewStop);
                this._silentRenewStop = null;

                this._OAuthService.stopCheckingSilentRenew();
            }
        });

    }

    private setAccessFromStorage() {

        let a = JSON.parse(localStorage.getItem('authorizationResult_' + environment.OAuth.config.client_id));
        let u = JSON.parse(localStorage.getItem('userData_' + environment.OAuth.config.client_id));

        if (a != null) {
            this.AuthData = a;
            this._accessToken = `${a['token_type']} ${a['access_token']}`;
            //console.log(this.AuthData);
        }
        if (u != null) this.User = u;
        //console.log(this.User);
    }


    public Authorize(): any {
        let authState: boolean;
        let authUser: AppUser;

        if (this._helper.isOnline) {
            return this._OAuthService.getIsAuthorized().pipe(
                takeUntil(timer(3000)),
                first((v, i, s) => {
                    //console.log('authState', v);
                    authState = v;
                    return v == true;
                }),
                concatMap(authState => {
                    this._isLoggedIn = authState;
                    //console.log('getUserData');
                    return this._OAuthService.getUserData()
                }),
                takeUntil(timer(3000)),
                first((v, i, s) => {
                    authUser = v;
                    return typeof v == "object";
                }),
                concatMap(authUser => {
                    return of(this.setUser(authUser));
                }),
                catchError(err => {
                    //console.log(err);
                    return throwError('Login Failed');
                })
            );
        }
        else {
            //console.log(this.AuthData && this.User);
            if (this.AuthData && this.User) {
                authState = true;
                this._isLoggedIn = authState;
                return of(this.setUser(this.User));
            }
            else return throwError('Offline Unauthorized');
        }
    }


    isAlive(): any {
        let authState: boolean;
        let authUser: AppUser;

        if (this._helper.isOnline) {
            return this._OAuthService.getIsAuthorized().pipe(
                concatMap(authState => {
                    //console.log(authState);
                    this._isLoggedIn = authState;
                    if (authState) return of(this.setUser(this.User));
                    else return this.Authorize();
                }),
                catchError(err => {
                    //console.log(err);
                    return throwError('Session Expired');
                })
            );
        }
        else {
            //console.log(this.AuthData && this.User);
            if (this.AuthData && this.User) {
                authState = true;
                this._isLoggedIn = authState;
                return of(this.setUser(this.User));
            }
            else return throwError('Offline Unauthorized');
        }



    }

    private setUser(user: any): AppUser {
        //console.log(user);
        this.User = user;
        this.setAccessFromStorage();
        this.AliveSubscriber.next(this.User);
        return this.User;
    }

    public login(): any {
        //.log('to Auth');
        this._OAuthService.authorize();
    }

    public logout(): any {
        this._ui.Loader = true;
        this._isLoggedIn = false;
        if (this._helper.isOnline) this.LogoutSubscriber.next(true);
        else {
            //console.log('logging out');
            this._OAuthService.logoff((x) => console.log(x));
            this._ui.Loader = false;
        }
        //this._OAuthService.logoff()
        //this._ui.Loader = false;
    }

    public getUser(state: boolean): Observable<AppUser> {
        //console.log('state',state);
        return this._OAuthService.getUserData().pipe(
            map(userData => {
                this.User = userData;
                this.AliveSubscriber.next(this.User);
                return this.User;
            })
        );
    }

    public setRedirectUrl(url: string = '/') {
        this._Local.store('redirect_uri', url);
    }

    public getRedirectUrl(): string {
        return this._Local.retrieve('redirect_uri') || '/';
    }

    public isLoggedIn() {
        return this._isLoggedIn;
    }

    private doCodeGrant() {
        // Will do a callback, if the url has a code and state parameter.
        //console.log(this._helper.QueryParams['code'], this._helper.QueryParams['access_token']);
        if (this._helper.QueryParams['code'] != undefined)
            this._OAuthService.authorizedCallbackWithCode(window.location.toString());
        if (this._helper.QueryParams['access_token'] != undefined) {
            this._OAuthService.authorizedImplicitFlowCallback();
        }
        if (!this._helper.isOnline) this._OAuthService.stopCheckingSilentRenew();
    }


}

export class AppUser {
    applicationId?: string;
    birthdate?: string;
    email?: string;
    email_verified?: boolean;
    family_name?: string;
    given_name?: string;
    name?: string;
    phone_number?: string;
    picture?: string;
    roles?: string[];
    sub?: string;
    preferred_username?: string;
}

