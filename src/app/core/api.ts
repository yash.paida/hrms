import { throwError as observableThrowError, of as observableOf, Observable, BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { LocalStorage } from '@ngx-pwa/local-storage';
import { map, catchError, tap, switchMap } from 'rxjs/operators';

//export enum AuthModes { Public, Anonymous, Secure }
export enum CacheStrategies { NoCache, Performance, Freshness, CacheFirst }
//export enum StorageTypes { Local, Session }

export type ParamConfig = { type: "param" | "json", key?: string }

export class ApiClient {
    private _prefix: string = "";
    private _Base_URI: string = "";
    //private _Headers: Headers[];
    private _Headers: any = {};
    private _RequestOptions: any = {};
    public ParamConfig: ParamConfig = { type: 'param' };

    private readonly commonHeaders = { "Content-Type": "application/json", "Accept": "application/json" };

    constructor(Prefix: string, BaseURI: string = '', private _http: HttpClient, private cache: LocalStorage) {

        this._prefix = Prefix;
        this._Base_URI = BaseURI;

        /* Default Headers */
        this.SetHeaders(this.commonHeaders);
    }

    clearCache$(url: string, filter?: any, ParamConfig: ParamConfig = this.ParamConfig) {
        let qp = "";
        qp = this.FiltertoQuery(filter, ParamConfig);
        let _url = url + (qp == "" ? "" : ((url.indexOf('?') >= 0 ? '&' : '?') + qp));
        let $Storage = this.cache;
        return $Storage.removeItem(this._prefix + "://GET " + _url);
    }
    get$(url: string, filter?: any, CacheStrategy: CacheStrategies = CacheStrategies.NoCache, errorHandler: ($e: any) => Observable<never> = this.handleError, ParamConfig: ParamConfig = this.ParamConfig): Observable<any> {
        let qp = "";
        qp = this.FiltertoQuery(filter, ParamConfig);
        let _url = url + (qp == "" ? "" : ((url.indexOf('?') >= 0 ? '&' : '?') + qp));
        let $Storage = this.cache;
        //console.log(url + " - " + url.indexOf("http"));
        let BaseURI = url.indexOf("http") >= 0 ? '' : this._Base_URI;

        if (!navigator.onLine) CacheStrategy = CacheStrategies.CacheFirst;

        //console.log(CacheStrategy);
        switch (CacheStrategy) {
            case CacheStrategies.NoCache: {
                return this._http.get(BaseURI + _url, this._RequestOptions).pipe(
                    map(response => <any>response),
                    catchError(errorHandler)
                );
            }
            case CacheStrategies.CacheFirst: {
                return $Storage.getItem(this._prefix + "://GET " + _url).pipe(
                    switchMap((value) => {
                        //console.log(value);
                        if (value) return observableOf(value)
                        else return this._http.get(BaseURI + _url, this._RequestOptions).pipe(
                            map(response => <any>response),
                            tap(data => $Storage.setItem(this._prefix + "://GET " + _url, data).subscribe()),
                            catchError(errorHandler)
                        );
                    })
                );
            }

            case CacheStrategies.Performance: {

                return $Storage.getItem(this._prefix + "://GET " + _url).pipe(
                    switchMap((value) => {
                        if (value) {
                            let bsres = new BehaviorSubject(value);
                            this._http.get(BaseURI + _url, this._RequestOptions)
                                .subscribe(response => {
                                    $Storage.setItem(this._prefix + "://GET " + _url, response).subscribe();
                                    bsres.next(response);
                                }, err => errorHandler);
                            return bsres.asObservable();
                        }
                        else return this._http.get(BaseURI + _url, this._RequestOptions).pipe(
                            map(response => <any>response),
                            tap(data => {
                                $Storage.setItem(this._prefix + "://GET " + _url, data).subscribe();
                            }),
                            catchError(errorHandler)
                        );
                    })
                );

            }
            case CacheStrategies.Freshness: {
                return this._http.get(BaseURI + _url, this._RequestOptions).pipe(
                    map(response => <any>response),
                    tap(data => {
                        $Storage.removeItem(this._prefix + "://GET " + _url).pipe(
                            tap(x => $Storage.setItem(this._prefix + "://GET " + _url, data).subscribe())
                        ).subscribe();
                    }),
                    catchError(errorHandler)
                );
            }

        }
        return this._http.get(BaseURI + _url, this._RequestOptions).pipe(
            map(response => <any>response),
            catchError(errorHandler)
        );
    }

    post$(url: string, model: any, headers?: any, errorHandler: ($e: any) => Observable<never> = this.handleError): Observable<any> {
        let _body: any;
        let isnewHeader = (headers != undefined);
        //console.log(model.constructor === Object);
        if (model.constructor === Object)
            _body = JSON.stringify(model);
        else _body = model;
        let _url = this._Base_URI + url;

        let OriginalHeaders: any;
        let NewHeaders: any;
        let NewRO: any;
        if (isnewHeader) {
            OriginalHeaders = this._Headers
            NewHeaders = Object.assign({}, headers, OriginalHeaders);
            NewRO = { headers: new HttpHeaders(NewHeaders) };
        }
        return this._http.post(_url, _body, isnewHeader ? NewRO : this._RequestOptions).pipe(
            map(response => <any>response),
            catchError(errorHandler)
        );

    }

    put$(url: string, model: any, errorHandler: ($e: any) => Observable<never> = this.handleError): Observable<any> {
        let _body = JSON.stringify(model);
        let _url = this._Base_URI + url;

        return this._http.put(_url, _body, this._RequestOptions).pipe(
            map(response => <any>response),
            catchError(errorHandler));
    }

    delete$(url: string, model: any, errorHandler: ($e: any) => Observable<never> = this.handleError): Observable<any> {
        let _body = JSON.stringify(model);
        let _url = this._Base_URI + url;
        let requestOption = Object.assign({}, this._RequestOptions, { 'body': _body });
        return this._http.delete(_url, requestOption).pipe(
            map(response => <any>response),
            catchError(errorHandler)
        );
    }

    public handleError(error: any) {
        console.error(error);
        //return Observable.throw(error.json().error || 'Server error');
        let response = error.text().toString()
        return observableThrowError(response);
    }

    public SetHeaders(AuthHeaders: any): void {
        //console.log();
        //Object.assign({},this._Headers,AuthHeaders)
        //this._Headers = AuthHeaders;
        //console.log(AuthHeaders);
        this._Headers = Object.assign({}, this._Headers, AuthHeaders);
        this._RequestOptions = { headers: new HttpHeaders(this._Headers) };


    }
    public ResetHeaders(): void {
        this._Headers = this.commonHeaders;
        this.SetHeaders(this._Headers);
    }

    private JSONtoParams(obj: any) {
        let params = new URLSearchParams();
        for (let key in obj) {
            params.set(key, obj[key])
        }
        return params;
    }
    private FiltertoQuery(obj: any, ParamConfig: ParamConfig = this.ParamConfig): string {
        let qp: string = "";

        if (ParamConfig.type == "param") {
            if (obj) qp = this.JSONtoParams(obj).toString();
        }
        else if (ParamConfig.type == "json") {
            if (obj) qp = ParamConfig.key + "=" + JSON.stringify(obj);
        }
        else qp = "";

        return qp;
    }
}
