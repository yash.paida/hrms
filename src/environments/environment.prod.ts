export const environment = {
  production: true,
  api:{
    gbase: {url : 'https://gbase.api.gurukul.ninja', headers:{}},
    authorization: {url : 'https://auth.api.gurukul.ninja', headers: { APIKey: "obsrRez2d0qOyp95V66rjg==" } },
    data: {url : 'https://gbase.api.gurukul.ninja', headers:{}},
    common: {url : 'https://gbase.api.gurukul.ninja/api/common', headers:{}},
  },
  OneSignal: {
    api: "https://onesignal.com/api/v1/",
    appId: "a0253cb8-3fa5-4f0e-b859-b2a9f92ea234",
    restKey: "OWZjM2VmZDctNjljYi00OWY1LTlhN2ItMzU3ODAzZjBhNDZk",
  },
  OAuth: {
    config: {
      "stsServer": "https://login.gurukul.ninja",
      "redirect_url": "https://dt.gurukul.ninja/auth",
      "client_id": "f336aac2-16a7-430b-80a8-533d82883385",
      "response_type": "code",
      "scope": "dataEventRecords securedFiles openid profile offline_access",
      "post_logout_redirect_uri": "https://dt.gurukul.ninja",
      "start_checksession": false,
      "silent_renew": true,
      "silent_renew_url": "https://dt.gurukul.ninja/assets/auth/silent-renew.html",
      "post_login_route": "/auth",
      "forbidden_route": "/auth/forbidden",
      "unauthorized_route": "/auth/unauthorized",
      "log_console_warning_active": false,
      "log_console_debug_active": false,
      "max_id_token_iat_offset_allowed_in_seconds": 10,
      "isauthorizedrace_timeout_in_seconds" : 10,
      "storage": localStorage,
    },
    authWellKnownEndpoints: {
      url: '{stsServer}/.well-known/openid-configuration',
      issuer: 'https://gurukul.ninja',
      jwks_uri: '{stsServer}/.well-known/jwks.json',
      authorization_endpoint: '{stsServer}/oauth2/authorize',
      token_endpoint: '{stsServer}/oauth2/token',
      userinfo_endpoint: '{stsServer}/oauth2/userinfo',
      end_session_endpoint: '{stsServer}/oauth2/logout?client_id={client_id}',
      check_session_iframe: '{stsServer}/oauth2/token',
      revocation_endpoint: '{stsServer}/oauth2/logout?client_id={client_id}',
      introspection_endpoint: '{stsServer}/oauth2/introspect'
    }
  },
  app: {
    prefix: 'pwa',
    version: require('../../package.json').version || '0.0.0',
    author: require('../../package.json').author || { name: "Gurukul Arts", email: "arts@gurukul.org" },
    repository: require('../../package.json').repository || { type: "git", url: "https://gitlab.com/GurukulArtsSeva/gpwa-template" },
    short_name: require('json-loader!../manifest.webmanifest').short_name || 'Angular PWA',
    name: require('json-loader!../manifest.webmanifest').name || 'Angular 8 PWA',
    description: require('json-loader!../manifest.webmanifest').description || 'My App for Angular 8 PWA',
    start_url: require('json-loader!../manifest.webmanifest').start_url || '/',
    scope: require('json-loader!../manifest.webmanifest').scope || '/',
    display: require('json-loader!../manifest.webmanifest').display || 'standalone',
    orientation: require('json-loader!../manifest.webmanifest').orientation || 'portrait',
    icons: require('json-loader!../manifest.webmanifest').icons,
    theme_color: require('json-loader!../manifest.webmanifest').theme_color || '#a7437c',
    background_color: require('json-loader!../manifest.webmanifest').background_color || '#fff',
  }
};
